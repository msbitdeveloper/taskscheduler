﻿using Microsoft.Extensions.Configuration;
using System;
using System.IO;

namespace MixedPlaceTaskScheduler
{
    class Program
    {
        static void Main(string[] args)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true);

            IConfigurationRoot configuration = builder.Build();
            TaskScheduler.Run(configuration);


        }
    }
}
