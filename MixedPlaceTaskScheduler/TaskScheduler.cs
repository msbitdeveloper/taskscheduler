﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace MixedPlaceTaskScheduler
{
    public class TaskScheduler
    {
        private const int oneSec = 1000;
        private const int oneMin = 60000;
        private const int tenMin = 600000;
        private const int oneHour = 3600000;

        public static void Run(IConfigurationRoot configuration)
        {
            #region Configurations
            var notificationsTimeInteraval = int.Parse(configuration["SchedulerTimeInteral:Notifications"]);
            var ntiMilSec = notificationsTimeInteraval * oneHour;
            var notificationEndpointUrl = configuration["Endpoints:NotificationAPI"];
            #endregion Configurations

            #region Start Tasks
            var notificationTask = Task.Factory.StartNew(() => CallNotificationAPI(notificationEndpointUrl, ntiMilSec));
            #endregion Start Task

            #region Failure Recovery
            while (true)
            {
               
                if (notificationTask.IsFaulted)
                {
                    notificationTask = Task.Factory.StartNew(() => CallNotificationAPI(notificationEndpointUrl, ntiMilSec));
                }
                Task.Delay(tenMin).Wait();
            }
            #endregion Failure Recovery

        }

        private static async void CallNotificationAPI(string endPointUrl, int timeInterval)
        {
            while (true)
            {
                Task.Delay(timeInterval).Wait();
                using (var httpClient = new HttpClient())
                {
                    var response = await httpClient.GetAsync(endPointUrl);
                }

            }

        }
    }
}
